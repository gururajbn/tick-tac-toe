/**
 * This program is a boliler plate code for the famous tic tac toe game
 * Here box represents one placeholder for either X or a 0
 * We have a 2D array to represent the arrangement of X or O is a grid
 * 0 -> empty box
 * 1 -> box with X
 * 2 -> box with O
 * 
 * Below are the tasks which needs to be completed
 * Imagine you are playing with Computer so every alternate move should be by Computer
 * X -> player
 * O -> Computer
 * 
 * Winner has to be decided and has to be flashed
 * 
 * Extra points will be given for the Creativity
 * 
 * Use of Google is not encouraged
 * 
 */
const grid = [];
const GRID_LENGTH = 5;
let cellCount=0;
let turn = 'X';
let draw = false;

function initializeGrid() {
    for (let colIdx = 0;colIdx < GRID_LENGTH; colIdx++) {
        const tempArray = [];
        for (let rowidx = 0; rowidx < GRID_LENGTH;rowidx++) {
            tempArray.push(0);
        }
        grid.push(tempArray);
    }
}

function getRowBoxes(colIdx) {
    let rowDivs = '';
    
    for(let rowIdx=0; rowIdx < GRID_LENGTH ; rowIdx++ ) {
        let additionalClass = 'darkBackground';
        let content = '';
        const sum = colIdx + rowIdx;
        if (sum%2 === 0) {
            additionalClass = 'lightBackground'
        }
        const gridValue = grid[colIdx][rowIdx];
        if(gridValue === 1) {
            content = '<span class="cross">X</span>';
        }
        else if (gridValue === 2) {
            content = '<span class="cross">O</span>';
        }
        rowDivs = rowDivs + '<div colIdx="'+ colIdx +'" rowIdx="' + rowIdx + '" class="box ' +
            additionalClass + '">' + content + '</div>';
    }
    return rowDivs;
}

function getColumns() {
    let columnDivs = '';
    for(let colIdx=0; colIdx < GRID_LENGTH; colIdx++) {
        let coldiv = getRowBoxes(colIdx);
        coldiv = '<div class="rowStyle">' + coldiv + '</div>';
        columnDivs = columnDivs + coldiv;
    }
    return columnDivs;
}

function renderMainGrid() {
    const parent = document.getElementById("grid");
    const columnDivs = getColumns();
    parent.innerHTML = '<div class="columnsStyle">' + columnDivs + '</div>';
}

function onBoxClick() {
    var rowIdx = this.getAttribute("rowIdx");
    var colIdx = this.getAttribute("colIdx");
    let newValue = 1;
    grid[colIdx][rowIdx] = newValue;
    renderMainGrid();
    addClickHandlers();
    checkGrid();
    computerTurn();
}

function addClickHandlers() {
    var boxes = document.getElementsByClassName("box");
    for (var idx = 0; idx < boxes.length; idx++) {
        boxes[idx].addEventListener('click', onBoxClick, false);
    }
}

function removeClickHandlers() {
    // remove handlers form box
    var boxes = document.getElementsByClassName("box");
    for (var idx = 0; idx < boxes.length; idx++) {
        boxes[idx].removeEventListener('click', onBoxClick, false);
    }
}

function rotateTurn(){
    //keep track of player
    if(turn==='X'){
        turn='O'
    }else{
        turn='X'
    }
}

function computerTurn(){
    for(i=0;i<GRID_LENGTH;i++){
        for(j=0;j<GRID_LENGTH;j++){
            if (grid[i][j]===0){
                grid[i][j] = 2;
                renderMainGrid();
                addClickHandlers();
                checkGrid()
                return
            }
        }
    }
}

function isGameOver(){

    //check rows and columns
    for (let row=0, col=0; row < GRID_LENGTH && col < GRID_LENGTH; row++, col++){

        let rowSet = new Set()
        let colSet = new Set()

        // populate both sets with values in rows and columns
        for(var i = 0;i < GRID_LENGTH; i++){
            rowSet.add(grid[row][i])
            colSet.add(grid[i][col])
        }

        // check for row
        if(rowSet.size == 1 && !rowSet.has(0) ){
            return true
        }

        // check for column
        if(colSet.size == 1 && !colSet.has(0)){
            return true
        }
    }


    // Check diagonals
    // Create two sets for both diagonals
    let diagonalLR = new Set()
    let diagonalRL = new Set()

    for (let lr = 0, rl = GRID_LENGTH - 1; lr < GRID_LENGTH && rl >= 0; lr++, rl--){
        diagonalLR.add(grid[lr][lr])
        diagonalRL.add(grid[GRID_LENGTH - 1 - rl][rl])
    }

    //check diagonal top-left to bottom-right
    if(diagonalLR.size === 1 && !diagonalLR.has(0) ){
        return true
    }

    //check diagonal top-right to bottom-left
    if(diagonalRL.size === 1 && !diagonalRL.has(0)){
        return true
    }

    // check for draw
    if (cellCount >= GRID_LENGTH * GRID_LENGTH){
        draw=true
        return true
    }

    return false
}

function checkGrid(){
    //update cell count
    cellCount++;
    let over = isGameOver();
    if (over || draw){
        //disable grid
        removeClickHandlers();
    }

    // flash result
    if (over && !draw){
        alert(turn+" won");
    }else if(over && draw){
        alert("Its a draw!")
    }else{
        rotateTurn()
    }

}

initializeGrid();
renderMainGrid();
addClickHandlers();
